package org.iprd.idmtest;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class ConnectActivity extends AppCompatActivity {
    private Button mConnectBtn,mAddPersonBtn,mPOSBtn,mBackBtn,mNextBtn;
    private Spinner mPersonType;
    private TextView mPersonText;
    private EditText mID;
    private Context mCtx;
    private IdmTestScreen mCurrentState;
    final String CONNECT = "ConnectToIdentity";
    final String ADD_PERSON = "AddPerson";
    final String POS = "POS";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCtx = getApplicationContext();
        setContentView(R.layout.connect_screen);
        mCurrentState = IdmTestScreen.CONNECT_SCREEN;
        mPersonType = (Spinner) findViewById(R.id.personType);
        mPersonText= (TextView) findViewById(R.id.personTypeText);
        mID = (EditText) findViewById(R.id.dhID);
        mConnectBtn = (Button) findViewById(R.id.connectapibutton);
        mConnectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String identityJSON = "{ \"resourceType\":\"IdentityRequest\", \"reqID\":\"100\", \"reqTxt\":\"ConnectToIdentity\", \"Inputs\": { } }";
                //identityJSON = "asas";
                Intent sendIntent = new Intent();
                sendIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                sendIntent.setAction("org.iprd.identity.IdmService");
                sendIntent.putExtra("IdentityRequest",identityJSON);
                sendIntent.setType("text/json");
                Intent chooser = Intent.createChooser(sendIntent, "Identity svc");
                if (sendIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(chooser, 66);

                    /*mNextBtn = (Button) findViewById(R.id.nxtBtn);
                    mNextBtn.setBackgroundResource(R.drawable.blue_rounded_background);
                    mNextBtn.setEnabled(true);*/
                }
            }
        });

        mAddPersonBtn = (Button) findViewById(R.id.addbutton);
        mAddPersonBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String currCtx = SessionProperties.getSessionInstance(mCtx).getSessionCtx();
                String personType = "HCW";
                if (mPersonType.getSelectedItem() != null)
                    personType = mPersonType.getSelectedItem().toString();
                String identityJSON = "";
                if (SessionProperties.getSessionInstance(mCtx).getAppType().equals("BIOMETRIC_ONLY"))
                    identityJSON = "{ \"resourceType\":\"IdentityRequest\", \"reqID\":\"101\", \"reqTxt\":\"AddPerson\", \"Inputs\": { \"currCtx\":\""+currCtx+"\", \"personType\":\""+personType.toString()+"\", \"userID\":\""+mID.getText().toString()+"\"} }";
                else if (SessionProperties.getSessionInstance(mCtx).getAppType().equals("GENERAL_APP"))
                    identityJSON = "{ \"resourceType\":\"IdentityRequest\", \"reqID\":\"101\", \"reqTxt\":\"AddPerson\", \"Inputs\": { \"currCtx\":\""+currCtx+"\", \"personType\":\""+personType.toString()+"\"} }";

                Log.d("idJSON",identityJSON);
                Intent sendIntent = new Intent();
                sendIntent.setAction("org.iprd.identity.IdmService");
                sendIntent.putExtra("IdentityRequest",identityJSON);
                sendIntent.setType("text/json");
                Intent chooser = Intent.createChooser(sendIntent, "Identity svc");
                if (sendIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(chooser, 66);
                }
            }
        });

        mPOSBtn = (Button) findViewById(R.id.posbutton);
        mPOSBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String currCtx = SessionProperties.getSessionInstance(mCtx).getSessionCtx();
                String identityJSON = "{ \"resourceType\":\"IdentityRequest\", \"reqID\":\"102\", \"reqTxt\":\"HandlePOS\", \"Inputs\": {\"currCtx\":\""+currCtx+"\"} }";
                //\"currCtx\":\""+SessionProperties.getSessionInstance(mCtx).getSessionCtx()+"\"
                Log.d("idJSON",identityJSON);
                Intent sendIntent = new Intent();
                sendIntent.setAction("org.iprd.identity.IdmService");
                sendIntent.putExtra("IdentityRequest",identityJSON);
                sendIntent.setType("text/json");
                Intent chooser = Intent.createChooser(sendIntent, "Identity svc");
                if (sendIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(chooser, 66);
                }
            }
        });

        mNextBtn = (Button) findViewById(R.id.nxtBtn);
        mNextBtn.setBackgroundResource(R.drawable.grey_rounded_background);
        mNextBtn.setEnabled(false);
        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                decideNextScreen();
            }
        });

        mBackBtn = (Button) findViewById(R.id.backBtn);
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Intent i = new Intent(ConnectActivity.this,MainActivity.class);
                startActivity(i);
                finish();*/
              decidePreviousScreen();
            }
        });
    }

    private void decideNextScreen()
    {
       switch(mCurrentState)
       {
           case CONNECT_SCREEN:
               setAddPersonScreen();
               break;
           case ADD_PERSON_SCREEN:
               setPOSScreen();
               break;
           case POS_SCREEN:
               Intent i = new Intent(ConnectActivity.this,MainActivity.class);
               startActivity(i);
               finish();
               break;
           default:
               break;
       }
    }

    private void decidePreviousScreen()
    {
        switch (mCurrentState)
        {
            case CONNECT_SCREEN:
                Intent i = new Intent(ConnectActivity.this,MainActivity.class);
                startActivity(i);
                finish();
                break;
            case ADD_PERSON_SCREEN:
                setConnectScreen();
                break;
            case POS_SCREEN:
                setAddPersonScreen();
                break;
            default:
                break;
        }
    }

    private void setConnectScreen()
    {
        mCurrentState = IdmTestScreen.CONNECT_SCREEN;
        mAddPersonBtn.setVisibility(View.INVISIBLE);
        mConnectBtn.setVisibility(View.VISIBLE);
        mPOSBtn.setVisibility(View.INVISIBLE);
        mPersonType.setVisibility(View.INVISIBLE);
        mPersonText.setVisibility(View.INVISIBLE);
        mID.setVisibility(View.INVISIBLE);
        mBackBtn.setVisibility(View.VISIBLE);
        mNextBtn.setVisibility(View.VISIBLE);
    }

    private void setAddPersonScreen()
    {
        //check if context is obtained. (possible only if connect call was done)
        if (SessionProperties.getSessionInstance(mCtx).getSessionCtx() == null)
            Toast.makeText(this,"Error: Please connect to Identity first",Toast.LENGTH_LONG).show();
        else
        {
            mCurrentState = IdmTestScreen.ADD_PERSON_SCREEN;
            mAddPersonBtn.setVisibility(View.VISIBLE);
            mConnectBtn.setVisibility(View.INVISIBLE);
            mPOSBtn.setVisibility(View.INVISIBLE);
            mPersonType.setVisibility(View.VISIBLE);
            mPersonText.setVisibility(View.VISIBLE);
            if (SessionProperties.getSessionInstance(mCtx).getAppType().equals("BIOMETRIC_ONLY"))
                mID.setVisibility(View.VISIBLE);
            mBackBtn.setVisibility(View.VISIBLE);
            mNextBtn.setVisibility(View.VISIBLE);
        }
    }

    private void setPOSScreen()
    {
        //check if context is obtained. (possible only if connect call was done)
        if (SessionProperties.getSessionInstance(mCtx).getSessionCtx() == null)
            Toast.makeText(this,"Error: Please connect to Identity first",Toast.LENGTH_LONG).show();
        else
        {
            mCurrentState = IdmTestScreen.POS_SCREEN;
            mAddPersonBtn.setVisibility(View.INVISIBLE);
            mPOSBtn.setVisibility(View.VISIBLE);
            mConnectBtn.setVisibility(View.INVISIBLE);
            mPersonType.setVisibility(View.INVISIBLE);
            mPersonText.setVisibility(View.INVISIBLE);
            mID.setVisibility(View.INVISIBLE);
            mBackBtn.setVisibility(View.VISIBLE);
            mNextBtn.setVisibility(View.VISIBLE);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == 66) {
            if (data.hasExtra("IdentityResponse")) {
                String t = data.getExtras().getString("IdentityResponse");
                try {
                    JSONObject tOut =new JSONObject(t);
                    String apiType = tOut.getString("reqTxt");
                    if (apiType.equals(CONNECT)) {
                        String currCtx = tOut.getJSONObject("Outputs").getString("currCtx");
                        String currAPI = tOut.getJSONObject("Outputs").getString("currAPI");
                        SessionProperties.getSessionInstance(mCtx).setSessionCtx(currCtx.toString());
                        String outStr = "Context: "+currCtx+"  API: "+currAPI;
                        Toast.makeText(this,outStr,
                                Toast.LENGTH_LONG).show();

                        mNextBtn = (Button) findViewById(R.id.nxtBtn);
                        mNextBtn.setBackgroundResource(R.drawable.blue_rounded_background);
                        mNextBtn.setEnabled(true);

                    } else if (apiType.equals(ADD_PERSON)) {
                        String guid = tOut.getJSONObject("Outputs").getString("guid");
                        String success = tOut.getJSONObject("Outputs").getString("addDone");
                        String uid = "";
                        String outStr = "";
                        String appStr = SessionProperties.getSessionInstance(mCtx).getAppType();
                        Log.i("App type in onActivityRes", appStr);
                        if (appStr.equals("BIOMETRIC_ONLY")) {
                            uid = tOut.getJSONObject("Outputs").getString("userID");
                            outStr = "guid: "+guid+" UID: "+uid;
                            Log.d("Biometric outStr: ",outStr.toString());
                        } else if (appStr.equals("GENERAL_APP")) {
                            outStr = "guid: " + guid;
                            Log.d("General outStr: ",outStr.toString());
                        }


                        if (success.equals("false")) {
                            outStr = "Failure";
                        }
                        Toast.makeText(this,outStr,Toast.LENGTH_LONG).show();

                    } else if (apiType.equals(POS)) {
                        String posDone = tOut.getJSONObject("Outputs").getString("posDone");
                        String outStr = "";
                        if (posDone.equals("true")) {
                            outStr = "Proof of Svc done";
                            Log.d("POS","Done");
                        } else {
                            Log.e("POS","Failed");
                            outStr = "Proof of Svc failed";
                        }
                        Toast.makeText(this,outStr,Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Log.e("Error while reading JSON",e.toString());
                    e.printStackTrace();

                    Toast.makeText(this,"Failure in API call",
                            Toast.LENGTH_LONG).show();
                }
            }
            else
            {
                Toast.makeText(this,data.getDataString(),Toast.LENGTH_LONG).show();
            }
        }
        else if (resultCode == RESULT_CANCELED)
        {
            Toast.makeText(this,"Failure in API call. Please check the data you are sending",Toast.LENGTH_LONG).show();
        }
    }
}

