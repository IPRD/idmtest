package org.iprd.idmtest;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionProperties {
    private static SessionProperties mSessionInstance = null;
    private String mSessionCtx;
    private Context mCtx;
    private SharedPreferences mPref;
    private SharedPreferences.Editor mEdit;
    private SessionProperties() {
        mSessionCtx = "";
    }

    private SessionProperties(Context appCtx) {
        initSharedPrefs(appCtx);
    }

    private void initSharedPrefs(Context ctx)
    {
        mCtx = ctx;
        mPref = ctx.getSharedPreferences("IdMSession", Context.MODE_PRIVATE);
        mEdit = mPref.edit();
    }

    public static synchronized SessionProperties getSessionInstance(Context appCtx) {
        if (mSessionInstance == null)
            mSessionInstance = new SessionProperties(appCtx);
        return mSessionInstance;
    }

    public String getSessionCtx() {
        StringBuffer sessionCtx = new StringBuffer();
        sessionState(sessionCtx,true,true,"SessionCtx");
        return sessionCtx.toString();
    }

    /**
     *
     * @param newSessionCtx
     */
    public void setSessionCtx(String newSessionCtx) {
        StringBuffer sessionCtx = new StringBuffer();
        sessionCtx.append(newSessionCtx);
        sessionState(sessionCtx,false,true,"SessionCtx");
    }

    public String getAppType()
    {
        StringBuffer appType = new StringBuffer();
        sessionState(appType,true,true,"AppType");
        return appType.toString();
    }

    public void setAppType(String newAppType)
    {
        StringBuffer appType = new StringBuffer();
        appType.append(newAppType);
        sessionState(appType,false,true,"AppType");
    }

    private void sessionState(StringBuffer userName, boolean getFn,boolean hcwUser,String stateVar) {
        String dataKey = "";
        //following state vars are not tied to hcw/patient and are common
        if (stateVar.equals("Location") || stateVar.equals("POS") || stateVar.equals("SessionCtx"))
            dataKey = stateVar;
        if (hcwUser)
            dataKey = "hcw"+stateVar;
        else
            dataKey = "patient"+stateVar;

        if (getFn) {
            String exValue = "";
            try {
                userName.append(mPref.getString(dataKey, exValue));
            } catch (ClassCastException e) {
                userName.append(exValue);
            }
        } else {
            mEdit.putString(dataKey,userName.toString());
            mEdit.commit();
        }
        return;
    }


}
