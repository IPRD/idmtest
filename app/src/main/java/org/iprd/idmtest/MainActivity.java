package org.iprd.idmtest;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    final String CONNECT = "ConnectToIdentity";
    final String ADD_PERSON = "AddPerson";
    final String POS = "POS";

    private EditText ccTxt;
    private Spinner personTxt;
    private CheckBox genAppChkBox;
    private CheckBox bioAppChkBox;
    private Context mCtx;
    private String mAppTypeStr;
    private Button mPreferenceSettingBtn;
    int PREFERENCES=7655;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCtx = getApplicationContext();
        if (getAppConfig() != null)
            mAppTypeStr = getAppConfig();
        else
            mAppTypeStr = "GENERAL_APP";
        setContentView(R.layout.activity_main);
        //mPreferenceSettingBtn = (Button) findViewById(R.id.preferenceSettingBtn);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button proceedButton = (Button) findViewById(R.id.proceedBtn);
        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SessionProperties.getSessionInstance(mCtx).setAppType(mAppTypeStr);
                Intent i = new Intent(MainActivity.this,ConnectActivity.class);
                finish();
                startActivity(i);
            }
        });


    }

    private void updateLabel(EditText edittext,Calendar myCalendar) {
        //String myFormat = "MM/dd/yy"; //In which you need put here
        String myFormat = "dd/MM/yyyy"; //date format used in Africa.
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        edittext.setText(sdf.format(myCalendar.getTime()));
    }

    private void setButtonVisibility(int btnid, int visibility) {
        Button b = (Button) findViewById(btnid);
        b.setVisibility(visibility);
    }

    private String getAppConfig()
    {
        Map<String,String> cfgMap = new HashMap<String,String>();
        if(loadConfigMap(mCtx,cfgMap))
        {
            String appTypeStr = cfgMap.get("appType");
            Log.d("Apptype from map",appTypeStr.toString());
            return appTypeStr;
        }
        else
            return null;
    }

    private boolean loadConfigMap(Context ctx, Map<String,String> cfgMap)
    {
        String cfgMapStr = loadJSONFromAsset(ctx, R.string.app_config);
        if (cfgMapStr == null || cfgMapStr.length() == 0)
            return false;
        try {
            JSONObject cfgMapJSON = new JSONObject(cfgMapStr);
            String appType = cfgMapJSON.getJSONObject("idmConfig").getString("appType");
            cfgMap.put("appType",appType);
            return true;
        } catch (JSONException e) {
            Log.e("Error while reading JSON",e.toString());
            return false;
        }
    }

    /**
     * This function gives a JSON string from a given asset id.
     * @param context
     * @param id
     * @return : json string
     */
    public static String loadJSONFromAsset(Context context, int id) {
        String json = null;
        if (context == null)
            throw new IllegalArgumentException("Context unavailable");
        Resources res = context.getResources();
        try {
            InputStream is = res.getAssets().open(res.getString(id));
            int size = is.available();
            if (size == 0)
                return null;
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private String getAppType()
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        return sharedPreferences.getString("appType",mAppTypeStr);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == 66) {
            if (data.hasExtra("IdentityResponse")) {
                String t = data.getExtras().getString("IdentityResponse");
                try {
                    JSONObject tOut =new JSONObject(t);
                    String apiType = tOut.getString("reqTxt");
                    if (apiType.equals(CONNECT)) {
                        String currCtx = tOut.getJSONObject("Outputs").getString("currCtx");
                        String currAPI = tOut.getJSONObject("Outputs").getString("currAPI");
                        SessionProperties.getSessionInstance(mCtx).setSessionCtx(currCtx.toString());
                        String outStr = "Context: "+currCtx+"  API: "+currAPI;
                        setButtonVisibility(R.id.addbutton,View.VISIBLE);
                        Toast.makeText(this,outStr,
                                Toast.LENGTH_LONG).show();
                    } else if (apiType.equals(ADD_PERSON)) {
                        String guid = tOut.getJSONObject("Outputs").getString("guid");
                        String success = tOut.getJSONObject("Outputs").getString("addDone");
                        String uid = "";
                        String outStr = "";
                        Log.i("App type in onActivityRes", mAppTypeStr.toString());
                        if (mAppTypeStr.equals("BIOMETRIC_ONLY")) {
                            uid = tOut.getJSONObject("Outputs").getString("userID");
                            outStr = "guid: "+guid+" UID: "+uid;
                            Log.d("Biometric outStr: ",outStr.toString());
                        } else if (mAppTypeStr.equals("GENERAL_APP")) {
                            outStr = "guid: " + guid;
                            Log.d("General outStr: ",outStr.toString());
                        }


                        if (success.equals("true")) {
                            setButtonVisibility(R.id.posbutton,View.VISIBLE);
                        } else {
                            outStr = "Failure";
                        }
                        Toast.makeText(this,outStr,Toast.LENGTH_LONG).show();

                    } else if (apiType.equals(POS)) {
                        String posDone = tOut.getJSONObject("Outputs").getString("posDone");
                        String outStr = "";
                        if (posDone.equals("true")) {
                            outStr = "Proof of Svc done";
                            Log.d("POS","Done");
                        } else {
                            Log.e("POS","Failed");
                            outStr = "Proof of Svc failed";
                        }
                        Toast.makeText(this,outStr,Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Log.e("Error while reading JSON",e.toString());
                    e.printStackTrace();
                    Toast.makeText(this,"Failure in API call",
                            Toast.LENGTH_LONG).show();
                }

            }
            else
            {
                Toast.makeText(this,data.getDataString(),Toast.LENGTH_LONG).show();
            }
        }
        else if (resultCode == RESULT_CANCELED)
        {
            Toast.makeText(this,"Failure in API call. Please check the data you are sending",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
